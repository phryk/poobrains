window.onload = pooInit;

window.poobrains = {};
window.poobrains.life = {};
window.poobrains.life.cells = [];
window.poobrains.life.width = 20;
window.poobrains.life.height = 20;


function pooInit(){

    var life = document.createElement('div');
    life.id = 'life';

    for(x = 0; x < window.poobrains.life.width; x++){

        window.poobrains.life.cells[x] = [];

        for(y = 0; y < window.poobrains.life.height; y++){


            var cell = document.createElement('div');
            cell.className='cell';

            if(Math.random() >= 0.5){
                window.poobrains.life.cells[x][y] = true;
                cell.className += ' alive';
            } else {
                window.poobrains.life.cells[x][y] = false;
                cell.className += ' dead';
            }

            cell.setAttribute('data-x', x);
            cell.setAttribute('data-y', y);
            cell.addEventListener('mouseout', lifeFlip, false);
            life.appendChild(cell);
        }
    }

    body = document.getElementById('wrapper');
    body.appendChild(life);
    window.setInterval(lifeUpdate, 1500);
}


function lifeUpdate(){

    for(x = 0; x < window.poobrains.life.width; x++){

        for(y = 0; y < window.poobrains.life.height; y++){

            var cell = document.querySelector("[data-x='"+x+"'][data-y='"+y+"']");

            var alive = poobrains.life.cells[x][y];
            var alive_neighbors = 0;
            var r = [-1, 0, 1];
            for(a in r){
                var _x = x + r[a];
                for(b in r){
                    var _y = y + r[b];
                    if(
                        !(r[a] == 0 && r[b] == 0) && 
                        (_x in window.poobrains.life.cells && _y in window.poobrains.life.cells[_x]) &&
                        window.poobrains.life.cells[_x][_y]
                    ){
                        alive_neighbors++;
                    }
                }
            }

            if(alive && alive_neighbors < 2){

                window.poobrains.life.cells[x][y] = false;
                cell.className = 'cell dead';

            } else if(alive && alive_neighbors > 3){

                window.poobrains.life.cells[x][y] = false;
                cell.className = 'cell dead';

            } else if(!alive && alive_neighbors == 3){

                window.poobrains.life.cells[x][y] = true;
                cell.className = 'cell alive';
            }
        }
    }
}

function lifeFlip(){
    console.log('flip');
    value = !window.poobrains.life.cells[this.dataset.x][this.dataset.y];
    window.poobrains.life.cells[this.dataset.x][this.dataset.y] = value;

    if(value){
        this.className = 'cell alive';
    } else {
        this.className = 'cell dead';
    }

}
