# coding=utf-8
import peewee as d#atabase
from sys import maxint
from os.path import exists # to check whether the db exists for installation
from functools import wraps
from collections import OrderedDict as odict
from hashlib import sha512
from docutils.core import publish_parts
from docutils import nodes
from rst2html5 import HTML5Writer
from docutils import nodes
from docutils.parsers.rst import Directive, directives
from werkzeug.wrappers import Response
from werkzeug.datastructures import MultiDict
from flask import Flask, Blueprint, request, session, render_template, flash, abort, redirect, url_for, g
from jinja2 import TemplateNotFound
import wtforms as wtf
import flask.ext.wtf as wtfe
from flask.ext.sass import Sass
from flask.ext.uploads import UploadSet 
from hookah import implements, invoke
import elements as el

#import pudb; pu.db

db = None

class Pooception(Exception):
    pass



class Writer(HTML5Writer):
    
    class video(nodes.Special, nodes.PreBibliographic, nodes.Element):
        """HTML5-specific "video" element."""
        pass



class MediaDirective(Directive):

    required_arguments = 1
    optional_arguments = 0
    final_argument_whitespace = True
    has_content = False
    option_spec = {
        'title': directives.unchanged
    }

    def run(self):

        reference = directives.uri(self.arguments[0])
        self.options['uri'] = reference

        image_node = nodes.image(rawsource=self.block_text, **self.options)

        video_node = Writer.video(rawsource=self.block_text, **self.options)

        print "DEM VIDEO NODE: ", video_node

        print "OPTIONS:", self.options

        return [image_node]


directives.register_directive('media', MediaDirective)


class Poobrains(Flask):

    def __init__(self, *args, **kwargs):

        super(Poobrains, self).__init__(*args, **kwargs)

        if not self.config.has_key('jinja_autoescape'):
            self.config['jinja_autoescape'] = ()#('.jinja')

    
    def select_jinja_autoescape(self, filename):

        if filename is None:
            return False

        return filename.endswith(self.config['jinja_autoescape'])

    
    def run(self, *args, **kwargs):

        self.register_blueprint(site)
        self.register_blueprint(admin, url_prefix="/admin")
        Sass(self)

        return super(Poobrains, self).run(*args, **kwargs)



class Pooprint(Blueprint):
    
    bases = None
    content_types = None

    def __init__(self, *args, **kwargs):

        super(Pooprint, self).__init__(*args, **kwargs)

    
    def register(self, app, options, first_registration=False):

        super(Pooprint, self).register(app, options, first_registration)
        self.config = app.config

        self.bases = get_bases()
        self.content_types = Content.get_children()




class Button(object):

    html_params = staticmethod(wtf.widgets.html_params)

    def __init__(self, button_type=None):
        if button_type is not None:
            self.button_type = button_type

    def __call__(self, field, **kwargs):
        kwargs.setdefault('id', field.id)
        kwargs.setdefault('type', self.button_type)
        if 'value' not in kwargs:
            kwargs['value'] = field.name

        return wtf.widgets.HTMLString('<button %s>%s</button>' % (self.html_params(name=field.name, **kwargs), field.label.text))



class SubmitButton(Button):

    button_type = 'submit'



class SubmitField(wtf.SubmitField):

    widget = SubmitButton()



# prepare for takeoff

app = Poobrains('poobrains')
import config


# connect to db
if app.config['db_type'] == 'sqlite':
    db = d.SqliteDatabase(app.config['db_name'], threadlocals=True)

elif app.config['db_type'] == 'postgres':
    db = d.PostgresqlDatabase(app.config['db_name'], user=app.config['db_user'], password=app.config['db_password'], threadlocals=True)



# some utility functions
def curry(func, *arg, **kwarg):

    """
    Implements currying with kwargs and stuff.
    """

    def call(*args, **kwargs):

        a = list(arg) #create a local copy rather than keeping a reference
        kw = dict(kwarg)
        args = list(args)
        while len(a):
            args.insert(0, a.pop())

        for k, v in kw.iteritems():
            if not kwargs.has_key(k): #only add kwarg if it's not already set, allows for overriding curried kwargs
                kwargs[k] = v

        return func(*args, **kwargs)

    return call



def trim(docstring):
    if not docstring:
        return ''
    # Convert tabs to spaces (following the normal Python rules)
    # and split into a list of lines:
    lines = docstring.expandtabs().splitlines()
    # Determine minimum indentation (first line doesn't count):
    indent = maxint
    for line in lines[1:]:
        stripped = line.lstrip()
        if stripped:
            indent = min(indent, len(line) - len(stripped))
    # Remove indentation (first line is special):
    trimmed = [lines[0].strip()]
    if indent < maxint:
        for line in lines[1:]:
            trimmed.append(line[indent:].rstrip())
    # Strip off trailing and leading blank lines:
    while trimmed and not trimmed[-1]:
        trimmed.pop()
    while trimmed and not trimmed[0]:
        trimmed.pop(0)
    # Return a single string:
    return '\n'.join(trimmed)


def get_bases():

    bases = odict()

    for name, cls in  Administerable.get_children().iteritems():
        if Administerable in cls.__bases__:
            bases[cls.__name__.lower()] = cls

    return bases



def render(page=None, template='main'):

    if page is None:
        page = g.page

    templates = [template+'.jinja']

    return render_template(templates, page=page)



def view(f):

    """
    Decorator to enable page functions to just return a renderable dictionary
    """
    @wraps(f)
    def wrapper(*args, **kwargs):

        invoke('regions_populate') # TODO: implement this somewhere

        value = f(*args, **kwargs)
        if type(value) is Response:
            return value

        content_set(value)


        return render(g.page)

    return wrapper



def breadcrumb(items_or_callable):

    def decorator(func):

        @wraps(func)
        def c(*a, **kw):

            if callable(items_or_callable):
                g.page['regions']['menus']['breadcrumb'] = items_or_callable(*a, **kw)

            else:
                g.page['regions']['menus']['breadcrumb'] = el.menu(items_or_callable)

            return func(*a, **kw)

        return c

    return decorator



def access(*args, **kwargs):

    """
    Decorator; Sets the permission needed to access this page.

    Alternatively, a custom access callback returning True or False might be
    passed.


    Example, using a permission name to determine access rights::

        @app.route('/foo')
        @access('access_foo')
        @view
        def page_foo():
            return value('Here be page content.')


    Example, using a custom access callback to determine access rights::

        def access_anon(user):
            if(user.id == 0):
                return True
            return False

        @app.route('/only/for/anon')
        @access(callback=access_anon)
        @view
        def page_anon():
            return (value('Only anon visitors get access to this.')
      

    ..  warning::
        
        This decorator has to be the below the app.route decorator for the page callback.


    ..  todo::
        
        Check if the custom callback example actually works
    """

    def decorator(func):

        @wraps(func)
        def c(*a, **kw):

            params = {'args': a, 'kwargs': kw}

            kwargs['params'] = params


            if g.user.access(*args, **kwargs):
                return func(*a, **kw)
            else:
                abort(401, "Not authorized for access.")        
        return c

    return decorator


def access_base(user, action='view', *args, **kwargs):

    """
    Determine whether a user has access to a certain poobrains base type.
    """

    if 'base' not in request.view_args.keys():
        return False

    base = request.view_args['base']

    if not base in admin.bases.keys():
        abort(404, 'Unknown base.')

    perm = '%s_%s' % (action, base)
    return user.access(perm)



def access_child(user, action='view', *args, **kwargs):

    """
    Determine whether a user has access to a certain poobrains child type.
    """
    if 'base' not in request.view_args.keys():
        return False

    if 'child' not in request.view_args.keys():
        return False

    base = request.view_args['base']
    child = request.view_args['child']

    if not base in admin.bases.keys():
        abort(404, 'Unknown base.')

    base_cls = admin.bases[base]

    if not child in base_cls.get_children().keys():
        abort(404, 'Unknown child.')

    perm = '%s_%s' % (action, child)
    return user.access(perm)



def access_content_type(user, action='view', *args, **kwargs):
    
    """
    Determine whether a user has access to a certain action of a content type.
    """

    if 'content_type' not in request.view_args.keys():
        return False

    content_type = request.view_args['content_type']

    if not content_type in admin.content_types.keys():
        abort(404, 'Unknown content type.')

    perm = '%s_%s' % (action, content_type)
    return user.access(perm)



def tagcrumb(*args, **kwargs):

    return el.text("tags'n'shit")



def set_title(title):
    g.page['title'] = title



def block_set(region, block, value):
    g.page['regions'][region][block] = value



def content_set(value):
    block_set('content', 'content', value)



def regions_build():

    regions = {}

    #add a few standard regions we can rely upon to exist
    regions['content'] = {}
    regions['messages'] = {}
    regions['menus'] = {}

    return regions



def installed():

    """
    Check whether the site is installed.

    ..TODO:: Differentiate between 'not installed' and 'a bunch of tables are missing'.
    """

    for model in PooModel.get_children().itervalues():

        if not model.table_exists():
            return False

    return True






def admin_instance_list(cls):

    is_base = cls in admin.bases.itervalues()

    instances_head = ['id', 'title', 'edit', 'delete']
    instances_rows = []

    if is_base:

        base = cls.__name__.lower()

        for row in cls.select():
            instances_rows.append([
                str(row.id),
                row.get_title(),
                el.link(url_for('.admin_base_edit', base=base, id=row.id), 'edit'),
                el.link(url_for('.admin_base_delete', base=base, id=row.id), 'delete'),
            ])

    else:

        # Find this childs base
        for base, base_cls in admin.bases.iteritems():
            if cls in base_cls.get_children().itervalues():
                break

        child = cls.__name__.lower()

        for row in cls.select():
            instances_rows.append([
                str(row.id),
                row.get_title(),
                el.link(url_for('.admin_child_edit', base=base, child=child, id=row.id), 'edit'),
                el.link(url_for('.admin_child_delete', base=base, child=child, id=row.id), 'delete'),
            ])

    return el.table(instances_rows, instances_head)





@implements('install')
def install(data):

    """
    Main installation procedure.
    """

    for model in PooModel.get_children().itervalues():
        model.create_table()

    super_perm = Permission()
    super_perm.name = 'super'
    super_perm.label = 'Super permission (allow everything)'
    super_perm.access = True
    super_perm.save()

    admin_perm = Permission()
    admin_perm.name = 'admin'
    admin_perm.label = 'View admin interface'
    admin_perm.access = True
    admin_perm.save()

    view_perms = []

    for name, base in get_bases().iteritems():

        perm = Permission()
        perm.name = 'view_%s' % (name,)
        perm.label = "View base type '%s'" % (base.__name__,)
        perm.access = True
        perm.save()
        view_perms.append(perm)

        perm = Permission()
        perm.name = 'manage_%s' % (name,)
        perm.label = "Manage base type '%s'" % (base.__name__,)
        perm.save()


    for name, content_type in Content.get_children().iteritems():

        perm = Permission()
        perm.name = 'view_%s' % (name,)
        perm.label = "View content of type '%s'" % (content_type.__name__,)
        perm.access = True
        perm.save()
        view_perms.append(perm)

        perm = Permission()
        perm.name = 'manage_%s' % (name,)
        perm.label = "Manage content of type '%s'" % (content_type.__name__,)
        perm.save()


    admin_group = Group()
    admin_group.name = 'admin'
    admin_group.label = 'Administrators'
    admin_group.permadd(admin_perm)
    admin_group.save()
    
    anon_group = Group()
    anon_group.name='reader'
    anon_group.label='Readers'

    for perm in view_perms:
        anon_group.permadd(perm)

    anon_group.save()

   
    admin = User()
    admin.name = data['name']
    admin.password = data['password']
    admin.groupadd(admin_group)
    admin.permadd(super_perm)
    admin.save()

    anon = User()
    anon.name = 'Anon'
    anon.password = 'i'
    anon.groupadd(anon_group)
    anon.save()


    session['uid'] = admin.id
    g.user = admin



@implements('regions_populate')
def main_menu():

    items = []
    
    g.page['regions']['menus']['main'] = el.menu(items, classes=['main'])


    # Build the fallback breadcrumb
    url = '/'
    pathargs = [x for x in request.path.split('/') if x != '']
    items = [
        el.link(url, app.config['site_name'])
    ]

    for arg in pathargs:
        url += arg+'/'
        items.append(el.link(url, arg))

    g.page['regions']['menus']['breadcrumb'] = el.menu(items, classes=['breadcrumb'])





class BoltOn(d.Model):
    
    name = None
    model = d.CharField()
    instance_id = d.IntegerField()


    def _get(self):

        raise NotImplementedError('BoltOn %s must implement its own _get.' % (self.__class__.__name__,))


    def _set(self, value):

        raise NotImplementedError('BoltOn %s must implement its own _set.' % (self.__class__.__name__,))



    def form(self):

        class BoltOnForm(wtf.Form):
            pass

        fields = self.__class__._meta.get_fields()

        for field in fields:
            if field.__class__ is not d.PrimaryKeyField and hasattr(field, 'form'):

                default = getattr(self, field.name)
                
                if field.__class__ is ForeignKeyField:
                    default = getattr(default, 'id', '_NULL')

                setattr(BoltOnForm, field.name, field.form(default))

        return BoltOnForm



    def form_handle(self, obj, values): # TODO: This is cloned from Administerable.fill, kill duplication!

        # Extract/preprocess form values (i.e. POST) into bolton_values.
        # This is done to enable clean form handling for BoltOn types

        prefix = 'bolton-'+self.name
        fields = self.__class__._meta.get_fields()
        bolton_values = MultiDict()

        for key in values.keys():

            if key.startswith(prefix):
                subkey = key[(len(prefix) + 1):] # remove prefix from key
                bolton_values[subkey] = values.get(key)


        for field in fields:

            if hasattr(field, 'form_handle') and callable(field.form_handle):
                field.form_handle(self, bolton_values)



# Database field classes with render and form functions

class PooField(object):


    def render(self, value):
        return el.Text(value)


    def form(self, default = None):

        return wtf.TextField(self.verbose_name or self.name, default=default)


    def form_handle(self, obj, form):

        if hasattr(obj, self.name) and self.name in form.keys() and form[self.name] != '':

            if form[self.name] == '_NULL':
                setattr(obj, self.name, None)
            else:
                setattr(obj, self.name, form[self.name])




class CharField(d.CharField, PooField):
    
    pass


class TextField(d.TextField, PooField):
    
    def form(self, default = None):
        return wtf.TextAreaField(self.verbose_name or self.name, default=default)



class PasswordField(d.CharField, PooField):
    
    def form(self, default = None):
        return wtf.PasswordField(self.verbose_name or self.name, default='')


class RstField(TextField):

    def db_value(self, value):

        if type(value) is RstString:
            return str(value)

        return value


    def python_value(self, *args, **kwargs):
        value = super(RstField, self).coerce(*args, **kwargs)
        return RstString(value)


    def form(self, default=None):

        if default is None:
            default = ''
        elif(type(default) is RstString):
            default = default.__repr__()

        return super(RstField, self).form(default=default)



class DateTimeField(d.DateTimeField, PooField):

    @classmethod
    def render(cls, value):
        return "DateTimeField: "+value


    def form(self, value = None):
        return "DateTimeField Form"



class IntegerField(d.IntegerField, PooField):

    @classmethod
    def render(cls, value):
        return "IntegerField: "+value


    def form(self, value = None):
        return "IntegerField Form"





class BooleanField(d.BooleanField, PooField):

    @classmethod
    def render(cls, value):
        return "BooleanField: "+value


    def form(self, value = None):
        return "BooleanField Form"




class FloatField(d.FloatField, PooField):

    @classmethod
    def render(cls, value):
        return "FloatField: "+value


    def form(self, value = None):
        return "FloatField Form"




class DoubleField(d.DoubleField, PooField):

    @classmethod
    def render(cls, value):
        return "DoubleField: "+value


    def form(self, value = None):
        return "DoubleField Form"





class BigIntegerField(d.BigIntegerField, PooField):

    @classmethod
    def render(cls, value):
        return "BigIntegerField: "+value


    def form(self, value = None):
        return "BigIntegerField Form"





class DecimalField(d.DecimalField, PooField):

    @classmethod
    def render(cls, value):
        return "DecimalField: "+value


    def form(self, value = None):
        return "DecimalField Form"





class ForeignKeyField(d.ForeignKeyField, PooField):

    @classmethod
    def render(cls, value):

        return "ForeignKeyField: "+value


    def form(self, default='_NULL'):

        choices = [('_NULL', 'None')]
        instances = self.rel_model.select()
        for instance in instances:
            choices.append((instance.id, instance.get_title()))

        return wtf.SelectField(self.verbose_name or self.name, choices=choices, default=default)






class DateField(d.DateField, PooField):

    @classmethod
    def render(cls, value):
        return "DateField: "+value


    def form(self, value = None):
        return "DateField Form"





class TimeField(d.TimeField, PooField):

    @classmethod
    def render(cls, value):
        return "TimeField: "+value


    def form(self, value = None):
        return "TimeField Form"





class BlobField(d.BlobField, PooField):

    """
    Remember to never use this.
    """

    @classmethod
    def render(cls, value):
        return "BlobField: "+value


    def form(self, value = None):
        return "BlobField Form"



class FileField(CharField):

    def form(self, default=None):

        print "filefield form"
        print default

        return wtf.FileField(self.verbose_name or self.name, default=default)




# Metaclasses


class MetaChildRegisterModel(d.BaseModel):

    """
    Using this metaclass will result in every model using it reporting itself
    to every parent model that defines _register_child().
    """

    _children = None
    _bolton_keys = None
    _boltons = None


    def __new__(mcs, name, bases, attrs):

        new_cls = super(MetaChildRegisterModel, mcs).__new__(mcs, name, bases, attrs)

        for b in bases: #register child class in all base classes supporting it
            if hasattr(b, '_register_child'):
                b._register_child(new_cls)

        for key in dir(new_cls):

            attr = getattr(new_cls, key)

            if hasattr(attr, '__class__') and issubclass(attr.__class__, BoltOn):
                attr.name = key
                attr.model = new_cls.__name__.lower()

        return new_cls


    def __init__(cls, name, bases, attrs):

        cls._children = odict()
        cls._boltons = []

        for key in dir(cls):

            attr = getattr(cls, key)

            if hasattr(attr, '__class__') and issubclass(attr.__class__, BoltOn):
                cls._boltons.append(key)

        super(MetaChildRegisterModel, cls).__init__(name, bases, attrs)


    def _register_child(cls, child):

        cls._children[child.__name__.lower()] = child
        
        for base in cls.__bases__:
            if hasattr(base, '_register_child'):
                base._register_child(child)


    def get_children(cls):
        return cls._children


# Other weird shit

def class_url(cls):
    return '/classurl'


def instance_url(self, mode='edit'):

    mro = self.__class__.__mro__

    if mode == 'view':

        if self.__class__ is Tag:

            url_components = [tag.url_component for tag in self.hierarchy()]
            return url_for('site.site_filter', filterpath='/'.join(url_components))

        elif Content in mro:

            if self.primary_tag:
                url_components = [tag.url_component for tag in self.primary_tag.hierarchy()]

            else:
                url_components = []

            #url_components.append(self.url_component)
            return url_for('site.site_filter', filterpath='/'.join(url_components))


    elif mode == 'edit':

        if Administerable in self.__class__.__bases__:
            return url_for('admin.admin_base_edit', base=self.__class__.__name__.lower(), id=self.id)

        else:

            try:
                idx = mro.index(Administerable)

            except ValueError as e:
                raise Pooception('Developer apparently bound UrlDescriptor to non-Administerable class. Original error: %s' % (e,))

            snip = mro[0:idx]
            return url_for('admin.admin_child_edit', base=snip[idx - 1].__name__.lower(), child=self.__class__.__name__.lower(), id=self.id)

    
    raise Pooception('Failure in instance_url function. Developer probably forgot to supply a fitting instance_func to the UrlDescriptor attribute of class %s.' % (self.__class__.__name__,))



class UrlDescriptor(object):

    """
    Magic descriptor attribute
    Returns a function to get the URL of a class or instance,
    based on whether its accessed from a class or instance.
    """

    class_func = None
    instance_func = None

    def __init__(self, class_func=class_url, instance_func=instance_url):

        self.class_func = class_func
        self.instance_func = instance_func


    def __get__(self, obj, objtype):

        if obj is None: # class method
            return curry(self.class_func, objtype)

        return curry(self.instance_func, obj) 




# Poobrains-specific models

class PooModel(d.Model):

    __metaclass__ = MetaChildRegisterModel

    class Meta:
        database = db


    @classmethod
    def get(cls, *args, **kwargs):

        row = super(PooModel, cls).get(*args, **kwargs)

        for bolton_name in row._boltons:
            bolton = getattr(row, bolton_name)
            bolton_model = bolton.__class__

            try:
                bolton_instance = bolton_model.get(model = row.__class__.__name__.lower(), instance_id = row.id)

            except bolton_model.DoesNotExist:

                bolton_instance = bolton_model()
                bolton_instance.model = row.__class__.__name__.lower()
                bolton_instance.instance_id = row.id

            bolton_instance.name = bolton_name

            setattr(row, bolton_name, bolton_instance)

        return row


    def save(self, *args, **kwargs):

        super(PooModel, self).save(*args, **kwargs)

        for bolton_name in self._boltons:

            bolton = getattr(self, bolton_name)

            # Set instance_id in case the 'parent' model instance was just inserted
            if not bolton.instance_id:
                bolton.instance_id = self.id

            bolton.save()


    def delete_instance(self, *args, **kwargs):

        for bolton_name in self._boltons:

            bolton = getattr(self, bolton_name)
            bolton.delete_instance()

        return super(PooModel, self).delete_instance(*args, **kwargs)





class Renderable(object):

    """
    Parent class for renderable objects.
    """

    #__metaclass__ = MetaChildRegister

    def render(self, display='teaser'):

        templates = []
        for cls in self.__class__.mro():
            templates.append('%s.jinja' % (cls.__name__,))

        value = {
            'display': display,
            'content': self
        }

        return {
            'template': templates,
            'value': value,
        } 






class UrlComponent(PooModel, BoltOn):
    
    url_component = CharField()

    
    def _get(self):
        return self.url_component


    def _set(self, value):

        self.url_component = value


class Filterable(PooModel, Renderable):

#    def render(self, display='teaser'):
#
#        try:
#            return render_template(self.__class__.__name__+".jinja", this=self)
#
#        except TemplateNotFound as e:
#
#            output = {
#                'head': ['field', 'value'],
#                'rows': []
#            }
#
#            fields = self.__class__._meta.get_fields()
#
#            for field in fields:
#                if field.__class__ is not d.PrimaryKeyField:
#                    output['rows'].append([field.name, field.__class__.render(getattr(self, field.name))])
#
#            return el.table(output)
    pass # TODO



class Administerable(Filterable):

    url = UrlDescriptor()

    def get_title(self):

        if hasattr(self, 'title'):
            return self.title

        if hasattr(self, 'label'):
            return self.label

        if hasattr(self, 'name'):
            return self.name

        return 'get_title needs to be defined for %s.' % (self.__class__.__name__,)


    def form(self):

        class AdminForm(wtf.Form):
            pass

        fields = self.__class__._meta.get_fields()



        for field in fields:
            if field.__class__ is not d.PrimaryKeyField and hasattr(field, 'form'):

                default = getattr(self, field.name)

                if field.__class__ is ForeignKeyField:
                    default = getattr(default, 'id', '_NULL')

                setattr(AdminForm, field.name, field.form(default))
        
        for bolton_name in self._boltons:

            bolton = getattr(self, bolton_name)

            setattr(AdminForm, 'bolton-'+bolton.name, wtf.FormField(bolton.form()))

        if len(fields):
            setattr(AdminForm, 'save', SubmitField('Save'))

        return AdminForm


    def fill(self, values):

        fields = self.__class__._meta.get_fields()

        for field in fields:
            if hasattr(field, 'form_handle') and callable(field.form_handle):

                field.form_handle(self, values)

        for bolton_name in self._boltons:
            
            bolton = getattr(self, bolton_name)
            bolton.form_handle(self, values)



class RenderString(object):

    _value = None

    def __init__(self, value):

        self._value = unicode(value)


    def __set__(self, value):
        self._value = unicode(value)


    def __len__(self):
        return len(self._value)


    def __unicode__(self):
        return self.render()

    def __str__(self):
        return self._value


    def __repr__(self):
        return self._value


    def split(self, *args, **kwargs):
        return self._value.split(*args, **kwargs)


    def render(self):
        return self._value




class RstString(RenderString):

    def render(self):

        writer = Writer()

        settings = {
            'input_encoding': 'UTF-8',
            'output_encoding': 'UTF-8',
        }

        rst = publish_parts(self._value, writer=writer, settings_overrides=settings)

        return rst['body']







class User(Administerable):

    name = CharField(unique=True)
    password = PasswordField()
    password_modified = None
    groups = None
    permissions = None

    
    def __init__(self, *args, **kwargs):

        super(User, self).__init__(*args, **kwargs)

        self.password_modified = False
        self.groups = {}
        self.permissions = {}


    def __setattr__(self, name, value):

        if name == 'password':
            self.password_modified = True

        super(User, self).__setattr__(name, value)


    def __repr__(self):

        if self.id is not None:
            return '<Poobrains User %d: %s>' % (self.id, self.name)

        return '<Poobrains User, unsaved>'


    def form(self):

        class PermissionForm(wtf.Form):
            pass

        class UserForm(wtf.Form):
            pass


        permopts = [
            ('grant', 'Grant'),
            ('ignore', 'Ignore (implicitly denies)'),
            ('deny', 'Deny')
        ]


        for perm in Permission.select():

            if perm.name in self.permissions.keys():
                perm.access = self.permissions[perm.name].access

            fval = lambda x: {True: 'grant', False: 'deny', None: 'ignore'}[x]
            default = fval(perm.access)

            setattr(PermissionForm, perm.name, wtf.RadioField(perm.label, choices=permopts, default=default))

        groupopts = []
        for group in Group.select():
            groupopts.append((group.id, group.label))

        groupdefs = []
        for name, group in self.groups.iteritems():
            groupdefs.append(group.id)

        UserForm.id = wtf.HiddenField('ID', default=self.id)
        UserForm.name = wtf.TextField('Name', default=self.name)
        UserForm.password = wtf.PasswordField('Password')

        UserForm.groups = wtf.SelectMultipleField('Groups', choices=groupopts, default=groupdefs)
        UserForm.permissions = wtf.FormField(PermissionForm)


        UserForm.save = SubmitField('Save')

        return UserForm

    
    def fill(self, values):

        super(User, self).fill(values)

        fval = {'grant': True, 'deny': False}

        self.permissions = {}
        for key in values.iterkeys():
            if key.startswith('permissions-') and values[key] != 'ignore':
                name = key[12:]

                try:
                    perm = Permission.get(Permission.name == name)
                except Permission.DoesNotExist as e:
                    raise e
                
                perm.access = fval[values[key]]
                self.permadd(perm)
                

        self.groups = {}
        for gid in values.getlist('groups'):
            try:
                group = Group.get(Group.id == gid)

            except Group.DoesNotExist as e:
                raise e

            self.groupadd(group)


    @classmethod
    def get(cls, *query, **kwargs):
        r = super(User, cls).get(*query, **kwargs)
        r.password_modified = False

        try:
            for group in r._groups:

                r.groupadd(group.group)

            for perm in r._permissions:
                perm.permission.access = bool(perm.access)
                r.permadd(perm.permission)

        except Exception as e:
            if app.config['DEBUG']:
                raise Pooception('Encountered a problem when loading groups and permissions of a user. Original error: %s' % (e,))


        r.authmap_build()

        return r


    def save(self):


        if not self.id or self.password_modified:
            self.password = sha512("%s%s" % (self.name, self.password)).hexdigest()

        try:
            super(User, self).save()

        except Exception as e:
            raise e

        else:

            UserGroup.delete().where(UserGroup.user == self).execute()
            for group in self.groups.itervalues():
                UserGroup.create(user=self, group=group)

            UserPermission.delete().where(UserPermission.user == self).execute()
            for perm in self.permissions.itervalues():
                if perm.access is not None:
                    UserPermission.create(user=self, permission=perm, access=perm.access)


    def groupadd(self, group):
        self.groups[group.name] = group


    def permadd(self, perm):
        self.permissions[perm.name] = perm


    def authmap_build(self):
        """
        Build the authorization map for this user.

        This function builds the authorization map for this user object based
        on its *perms* and *groups* properties.

        The authmap is a dictionary holding the users' access status for every
        single permission, denoted by either True (granted) or False (denied).

        The authmap is created as this user objects *authmap* property.
        """

        self.authmap = {}

        for perm in Permission.select():
            if perm.name in self.permissions.keys():

                self.authmap[perm.name] = self.permissions[perm.name].access

            else:

                for group in self.groups.itervalues():
                    if perm.name in group.permissions.keys():

                        permission = group.permissions[perm.name]

                        if not perm.name in self.authmap.keys() or permission.access == False:
                            self.authmap[perm.name] = permission.access


            if not perm.name in self.authmap.keys():
                self.authmap[perm.name] = False


    
    def auth_granted(self, perm, *args, **kwargs):

        """
        Check if a certain permission is granted to this user.

        :param perm: The name of the permission to check for
        :rtype: bool
        """

        if self.authmap.has_key(perm):
            return self.authmap[perm]

        if app.config['DEBUG']:
            raise LookupError(('No such permission: %s' % (perm)))
        flash(('Failed to authenticate because permission does not exist: %s' % (perm)), 'error')
        return False


    def access(self, *args, **kwargs):

        """
        Perform a permission check with the possibility of a custom callback.

        This function calls the callback supplied in ``**kwargs['callback']``
        if any, or self.auth_granted as a fallback.

        This mechanism exists in order to enable custom callbacks for the
        *@access* decorator which is used to authorize a user to see a page.
        """

        if self.authmap['super']:
            return self.authmap['super']

        if kwargs.has_key('callback'):
            args = list(args)
            args.insert(0, self)
            callback = kwargs['callback']
            del kwargs['callback']

        else:
            callback = self.auth_granted

        return callback(*args, **kwargs)


    @classmethod
    def passhash(cls, name, password):

        r = sha512("%s%s" % (name, password)).hexdigest()
        return r



class Group(Administerable):

    name = CharField(unique=True)
    label = CharField()

    def __init__(self, *args, **kwargs):
        
        super(Group, self).__init__(*args, **kwargs)

        self.permissions = {}


    def form(self):

        class PermissionForm(wtf.Form):
            pass

        class GroupForm(wtf.Form):
            pass


        permopts = [
            ('grant', 'Grant'),
            ('ignore', 'Ignore (implicitly denies)'),
            ('deny', 'Deny')
        ]


        for perm in Permission.select():

            if perm.name in self.permissions.keys():
                perm.access = self.permissions[perm.name].access

            fval = lambda x: {True: 'grant', False: 'deny', None: 'ignore'}[x]
            default = fval(perm.access)

            setattr(PermissionForm, perm.name, wtf.RadioField(perm.label, choices=permopts, default=default))


        GroupForm.name = wtf.TextField('Name', default=self.name)
        GroupForm.label = wtf.TextField('Label', default=self.label)
        GroupForm.permissions = wtf.FormField(PermissionForm)
        GroupForm.save = SubmitField('Save')

        return GroupForm


    def fill(self, values):

        super(Group, self).fill(values)

        fval = {'grant': True, 'deny': False}

        self.permissions = {}
        for key in values.iterkeys():
            if key.startswith('permissions-') and values[key] != 'ignore':
                name = key[12:]

                try:
                    perm = Permission.get(Permission.name == name)
                except Permission.DoesNotExist as e:
                    raise e
                
                perm.access = fval[values[key]]
                self.permadd(perm)
                


    @classmethod
    def get(cls, *query, **kwargs):

        r = super(Group, cls).get(*query, **kwargs)

        for perm in r._permissions:
            permission = perm.permission
            permission.access = bool(perm.access)
            r.permadd(permission)

        return r
  

    def save(self):

        try:
            super(Group, self).save()

        except Exception as e:
            raise e

        else:
            GroupPermission.delete().where(GroupPermission.group == self).execute()
            for perm in self.permissions.itervalues():
                if perm.access is not None:
                    GroupPermission.create(group=self, permission=perm, access=perm.access)


    def permadd(self, perm):
        self.permissions[perm.name] = perm



class Permission(PooModel):

    name = CharField(unique=True)
    label = CharField()

    def __init__(self, *args, **kwargs):

        super(Permission, self).__init__(*args, **kwargs)

        self.access = None



# Linker tables after this
class UserGroup(PooModel):

    user = ForeignKeyField(User, related_name='_groups')
    group = ForeignKeyField(Group, related_name='_users')



class UserPermission(PooModel):

    user = ForeignKeyField(User, related_name='_permissions')
    permission = ForeignKeyField(Permission, related_name='_users')
    access = IntegerField() # 0 = Deny, 1 = Allow



class GroupPermission(PooModel):

    group = ForeignKeyField(Group, related_name='_permissions')
    permission = ForeignKeyField(Permission, related_name='_groups')
    access = IntegerField() # 0 = Deny, 1 = Allow



class Media(Administerable):

    file = FileField()
    filetype = d.CharField()

    def fill(self, values):

        return super(Media, self).fill(values)


class Tag(Administerable):

    name = CharField(unique=True)
    parent = ForeignKeyField('self', related_name='children', null=True)
    description = RstField('Description')
    url_component = UrlComponent() # BoltOn Model


    def __repr__(self):

        return '<Poobrains Tag: %s>' % (self.name,)


    def save(self):

        self.url_component = self.name.lower()

        if self.depth() > app.config['max_tag_depth']:
            raise Pooception('Tag depth limit exceeded.')

        if self.id and self.parent and self.id == self.parent.id:
            raise Pooception('A Tag may not be its own parent.')

        super(Tag, self).save()


    def depth(self):

        if not self.parent:
            return 1

        return self.parent.depth() + 1


    def hierarchy(self):

        if not self.parent:
            return [self]

        hierarchy = self.parent.hierarchy()
        hierarchy.append(self)
        return hierarchy




class Content(Administerable):

    """
    Content base class. Instances of this class will not be visible on the site.

    * Rst
    * Test
    """

    title = CharField(verbose_name=u'Title')
    primary_tag = ForeignKeyField(Tag, null=True, verbose_name=u'Primary tag')
    body = RstField(verbose_name='Body')
    url_component = UrlComponent()

    secondary_tags = None

    def __init__(self, *args, **kwargs):

        super(Content, self).__init__(*args, **kwargs)
        self.secondary_tags = []


    @classmethod
    def get(cls, *query, **kwargs):


        obj =  super(Content, cls).get(*query, **kwargs)

        assocs = ContentTags.select().where(ContentTags.content_type == cls.__name__ and ContentTags.content_id == obj.id)

        for assoc in assocs:
            obj.secondary_tags.append(assoc.tag)

        return obj


    def save(self):

        super(Content, self).save()

        # clear current tag associations
        self.secondary_tags = []
        ContentTags.delete().where(ContentTags.content_type == self.__class__.__name__ and ContentTags.content_id == self.id).execute()

        # load tags corresponding to secondary_tags form field
        for tagid in request.form.getlist('secondary_tags'):
            tagid = int(tagid)

            try:

                tag = Tag.get(Tag.id == tagid)
                self.secondary_tags.append(tag)

            except Exception as e:

                flash("Couldn't associate content with tag %d, it might have been deleted in the meantime." % ((tagid,)), 'error')


        # write tag associations
        for tag in self.secondary_tags:
            assoc = ContentTags()
            assoc.content_type = self.__class__.__name__
            assoc.content_id = self.id
            assoc.tag = tag
            assoc.save()


    def form(self):

        form = super(Content, self).form()
        choices = []
        default = []

        for tag in Tag.select():
            choices.append((tag.id, tag.name))

        for tag in self.secondary_tags:
            default.append(tag.id)

        form.secondary_tags = wtf.SelectMultipleField('Tags', choices=choices, default=default)

        return form




class ContentTags(PooModel):

    content_type = d.CharField()
    content_id = d.IntegerField()
    tag = d.ForeignKeyField(Tag)

    @classmethod
    def get(cls, *query, **kwargs):

        return super(ContentTags, cls).get(*query, **kwargs)


    def save(self, force_insert=False, only=None):

        return super(ContentTags, self).save(force_insert=force_insert, only=only)



class LoginForm(wtf.Form):

    """
    Login Form.
    """

    name = wtf.TextField('User', validators=[wtf.validators.Required()])
    password = wtf.PasswordField('Password', validators=[wtf.validators.Required()])

    submit = SubmitField('Log in')



class FormDelete(wtf.Form):

    """
    Content deletion form.
    """

    delete = SubmitField('Kill it with fiyah!')



admin = Pooprint('admin', 'poobrains', template_folder='templates/admin')
site = Pooprint('site', 'poobrains', template_folder='templates/site')



# CRUD functions for use in page callbacks below this

def add_post(cls, values):
    obj = cls()
    obj.fill(values)
    try:
        obj.save()

    except Exception as e:

        flash("Could not add %s." % (cls.__name__,), 'error')
        flash("Error message: %s." % (e.message,), 'error')
        return redirect(cls.url())

    else:

        flash("%s instance '%s' has been added." % (cls.__name__, obj.get_title()))

        return redirect(obj.url())




# Request initialization and teardown

@site.url_value_preprocessor
@admin.url_value_preprocessor
def url_traces(endpoint, values):

    g.url_trace = {}
    g.url_trace['content_type'] = values['content_type'] if 'content_type' in values.keys() else None



@site.before_request
@admin.before_request
def request_init():

    try:
        db.connect()

    except d.OperationalError as e:
        abort(500, "Couldn't connect to database.")

    g.page = {}

    g.page['site_name'] = app.config['site_name']
    g.page['title'] = '';
    g.page['regions'] = regions_build()

    if not installed():

        if request.path != "/admin/install/": 
            abort(403, "Site not installed.")

    elif session.has_key('uid'):
        try:
            g.user = User.get(User.id == session['uid'])

        except User.DoesNotExist:
            session.pop('uid')
            g.user = User.get(User.id == 2)
    
    else:
        g.user = User.get(User.id == 2)
        session['uid'] = g.user.id



@site.after_request
@admin.after_request
def request_teardown(response):

    try:
        db.close()
    except Exception:
        pass

    return response




# Site routes

@site.route('/favicon.ico')
def site_favicon():
    return "TODO: Dynamic icon foo"


@site.route('/')
@view
def site_root():

    set_title('Site root')
    return "site root"




@site.route('/login')
@view
def site_login():

    set_title('Login')

    if session.has_key('uid') and session['uid'] != 2: # 2 is Anon
        
        return [
            el.text('Already logged in as %s.' % (g.user.name,)),
            el.link(url_for('.site_logout'), 'Logout?')
        ]
    return el.form(LoginForm())



@site.route('/login', methods=['POST'])
def site_login_submit():
    try:
        u = User.get(User.name == request.form['name'] and User.password == User.passhash(request.form['name'], request.form['password']))

        session['uid'] = u.id
        flash('Welcome, %s.' % u.name)
        return redirect(url_for('.site_root'))

    except User.DoesNotExist as e:
        flash('Username or password incorrect.', 'error')
        return redirect(url_for('.site_login'))


@site.route('/logout')
def site_logout():

    if session.has_key('uid') and session['uid'] != 2: # 2 being Anon
        name = g.user.name
        session.pop('uid')
        flash('Logged out %s.' % name)

    else:
        flash("You're not even logged in!", 'error')

    return redirect(url_for('.site_root'))
    


@site.route('/type/<string:content_type>/')
@view
@access(callback=curry(access_content_type, action='view'))
def site_content_type(content_type):

    cls = site.content_types[content_type]
    set_title(cls.__name__)

    posts = cls.select()
    teasers = []    
    for post in posts:
        teasers.append(post.render('teaser'))

    return el.ulist(teasers)


@site.route('/<path:filterpath>/')
@breadcrumb(tagcrumb)
@view
def site_filter(filterpath):

    pathargs = filterpath.split('/')
    if len(pathargs) > app.config['max_tag_depth'] + 1:
        abort(403, "Webmaster hasn't paid for the pro version of this software. Webmaster probably is an evil pirate bankrupting the industry!")

    else:

        for (index, url_component) in zip(range(0, len(pathargs)), pathargs):

            last_tag = None

            try:
                tag = Tag.get(Tag.url_component == url_component)

            except Tag.DoesNotExist as e:

                if index == len(pathargs) - 1:
                    print "Might be content!"

                else:
                    abort(404, "Found no tag for '%s'." % (url_component,))
                    #TODO: All working tags up to this

            else:

                last_tag = tag


    return el.text(filterpath)



# Admin routes below this

@admin.route('/install/')
@view
def admin_install():

    set_title('Installation')

    if installed():
        return "db exists, no install"
  
    class InstallForm(wtf.Form):
        name = wtf.TextField('Administrator name')
        password = wtf.PasswordField('Password')

    invoke('install_form', InstallForm)

    InstallForm.install = SubmitField('Install')

    form = InstallForm()
    return el.form(form)



@admin.route('/install/', methods=['POST'])
def admin_install_submit():

    if not installed():
        
        data = request.form

        if not len(data['name']) and not len(data['password']):
            flash('Name and password for the admin user must be supplied.', 'error')
            return redirect(url_for('.admin_install'))

        invoke('install', data)

        flash('Welcome to your new site, %s.' % (g.user.name))
        return redirect(url_for('.admin_root'))

    else:
        abort(403)


@admin.route('/')
@view
@access('admin')
def admin_root():

    set_title('Administration')

    items = []
    for name, base in admin.bases.iteritems():
        items.append(el.link(url_for('.admin_base', base=name), base.__name__))

    return el.menu(items)



# Base types

@admin.route('/<string:base>/')
@view
@access(callback=curry(access_base, action='manage'))
def admin_base(base):

    cls = admin.bases[base]

    set_title(cls.__name__)

    content = []

    if cls.__doc__:
        content.append(RstString(trim(cls.__doc__)))

    children = cls.get_children()
    child_links = []
    for name, child in children.iteritems():
        child_links.append(el.link(url_for('.admin_child', base=base, child=name), child.__name__))

    content.append(el.menu(child_links))
    content.append(admin_instance_list(cls))
    add = el.link(url_for('.admin_base_add', base=base), 'Add')

    return el.container(content, actions=[add])



@admin.route('/<string:base>/add')
@view
@access(callback=curry(access_base, action='manage'))
def admin_base_add(base):

    cls = admin.bases[base]

    set_title('Add %s' % (cls.__name__,))

    return el.form(cls().form()(request.form))



@admin.route('/<string:base>/add', methods=['POST'])
@access(callback=curry(access_base, action='manage'))
def admin_base_add_post(base):

    cls = admin.bases[base]

    return add_post(cls, request.form)


@admin.route('/<string:base>/<int:id>/')
@view
@access(callback=curry(access_base, action='manage'))
def admin_base_edit(base, id):

    cls = admin.bases[base]
    
    try:
        obj = cls.get(cls.id == id)
    except cls.DoesNotExist as e:
        flash('No %s instance with this id exists.' % (cls.__name__,), 'error')
        return redirect(url_for('.admin_base', base=base))
    
    set_title('Edit %s' % (obj.get_title(),))

    return el.form(obj.form()(request.form))



@admin.route('/<string:base>/<int:id>/', methods=['POST'])
@access(callback=curry(access_base, action='manage'))
def admin_base_edit_post(base, id):

    cls = admin.bases[base]

    try:
        obj = cls.get(cls.id == id)

    except cls.DoesNotExist as e:

        flash('No %s instance with this id exists.' % (cls.__name__,), 'error')
        return redirect(url_for('.admin_base', base=base))


    obj.fill(request.form)
    obj.save()

    flash("%s instance '%s' has been saved." % (cls.__name__, obj.get_title()))
    return redirect(url_for('.admin_base', base=base))



@admin.route('/<string:base>/<int:id>/delete/')
@view
@access(callback=curry(access_base, action='manage'))
def admin_base_delete(base, id):

    if base == 'user' and (id == 1 or id == 2):
        flash("Sorry %s, I can't do that." % (g.user.name,), 'error')
        return redirect(url_for('.admin_base', base=base))

    cls = admin.bases[base]

    try:
        obj = cls.get(cls.id == id)

    except cls.DoesNotExist as e:

        flash('No %s instance with this id exists.' % (cls.__name__,), 'error')
        return redirect(url_for('.admin_base', base=base))


    set_title('Delete %s' % (obj.get_title(),))
    items = []

    items.append(el.text("This will permanently delete %s instance '%s'." % (cls.__name__, obj.get_title())))
    items.append(el.form(FormDelete()))


    return el.container(items)



@admin.route('/<string:base>/<int:id>/delete/', methods=['POST'])
@access(callback=curry(access_base, action='manage'))
def admin_base_delete_post(base, id):

    cls = admin.bases[base]

    try:
        obj = cls.get(cls.id == id)

    except cls.DoesNotExist as e:

        flash('No %s instance with this id exists.' % (cls.__name__,), 'error')

    title = obj.get_title()
    if obj.delete_instance():
        flash("Successfully deleted '%s'." % (title,))
    else:
        flash("Could not delete '%s'." % (title,), 'error')
        return redirect(url_for('.admin_base_delete', base=base, id=id))

    return redirect(url_for('.admin_base', base=base))


# Child types (everything below bases)

@admin.route('/<string:base>/<string:child>/')
@view
@access(callback=curry(access_child, action='manage'))
def admin_child(base, child):

    cls = Administerable.get_children()[child]
    set_title(cls.__name__)

    add = el.link(url_for('.admin_child_add', base=base, child=child), 'Add')

    return el.container([admin_instance_list(cls)], actions=[add])



@admin.route('/<string:base>/<string:child>/add')
@view
@access(callback=curry(access_child, action='manage'))
def admin_child_add(base, child):

    cls = Administerable.get_children()[child]
    set_title('Add %s' % (cls.__name__,))

    return el.form(cls().form()(request.form))



@admin.route('/<string:base>/<string:child>/add', methods=['POST'])
#@access(callback=curry(access_child, action='manage'))
def admin_child_add_post(base, child):

    cls = Administerable.get_children()[child]

    return add_post(cls, request.form)



@admin.route('/<string:base>/<string:child>/<int:id>/')
@view
@access(callback=curry(access_child, action='manage'))
def admin_child_edit(base, child, id):

    cls = Administerable.get_children()[child]
    
    try:
        obj = cls.get(cls.id == id)
    except cls.DoesNotExist as e:
        flash('No %s instance with this id exists.' % (cls.__name__,), 'error')
        return redirect(url_for('.admin_child', base=base, child=child))

    set_title('Edit %s' % (obj.get_title(),))

    return el.form(obj.form()(request.form))



@admin.route('/<string:base>/<string:child>/<int:id>/', methods=['POST'])
@access(callback=curry(access_child, action='manage'))
def admin_child_edit_post(base, child, id):

    cls = Administerable.get_children()[child]

    try:
        obj = cls.get(cls.id == id)

    except cls.DoesNotExist as e:

        flash('No %s instance with this id exists.' % (cls.__name__,), 'error')
        return redirect(url_for('.admin_child', base=base, child=child))


    obj.fill(request.form)
    try:
        obj.save()
   
    except:

        if app.config['DEBUG'] is True:
            raise

        flash("Could not save %s instance '%s'." % (cls.__name__, obj.get_title()))
        return redirect(url_for('.admin_child_edit', base=base, child=child, id=id))

    else:
        flash("%s instance '%s' has been saved." % (cls.__name__, obj.get_title()))
        return redirect(url_for('.admin_child', base=base, child=child))



@admin.route('/<string:base>/<string:child>/<int:id>/delete/')
@view
@access(callback=curry(access_child, action='manage'))
def admin_child_delete(base, child, id):

    cls = Administerable.get_children()[child]

    try:
        obj = cls.get(cls.id == id)

    except cls.DoesNotExist as e:

        flash('No %s instance with this id exists.' % (cls.__name__,), 'error')
        return redirect(url_for('.admin_child', base=base, child=child))

    set_title('Delete %s' % (obj.get_title(),))

    items = []
    items.append(el.text("This will permanently delete %s instance '%s'." % (cls.__name__, obj.get_title())))
    items.append(el.form(FormDelete()))


    return el.container(items)



@admin.route('/<string:base>/<string:child>/<int:id>/delete/', methods=['POST'])
@access(callback=curry(access_child, action='manage'))
def admin_child_delete_post(base, child, id):

    cls = Administerable.get_children()[child]

    try:
        obj = cls.get(cls.id == id)

    except cls.DoesNotExist as e:

        flash('No %s instance with this id exists.' % (cls.__name__,), 'error')

    title = obj.get_title()
    if obj.delete_instance():
        flash("Successfully deleted '%s'." % (title,))
    else:
        flash("Could not delete '%s'." % (title,), 'error')
        return redirect(url_for('.admin_child_delete', base=base, child=child, id=id))

    return redirect(url_for('.admin_child', base=base, child=child))




@app.template_test()
def renderable(item):

    return hasattr(item, 'render') and callable(item.render)
