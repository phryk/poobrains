"""
Element shortcut module

..  todo::

    Create a class for the ``value`` variable to avoid issues with same
    property and key names (as with ``items``).
"""

from collections import OrderedDict as odict
from flask import request


def element(type, value, **kwargs):

    for key, val in kwargs.iteritems():
        if str(key).startswith('_'):
            key = key[1:]

        value[key] = val

    return  {
        'value': value,
        'template': type+'.jinja'
    }


def value(value):
    return element('value', value)


def text(value, **kwargs):

    v = odict()
    v['text'] = value

    return element('text', v, **kwargs)


def container(value, actions=None, **kwargs):

    v = odict()
    if not type(value) is list:
        value = [value]

    v['actions'] = actions
    v['children'] = value

    return element('container', v, **kwargs)


def link(url, label, **kwargs):

    value = {
        'url': url,
        'label': label,
    }

    return element('link', value, **kwargs)


def table(rows, header=None, **kwargs):

    value = {
        'rows': rows
    }

    if header:
        value['header'] = header

    return element('table', value, **kwargs)


def form(form, id = None, method = 'POST', action = None, enctype = None, **kwargs):

    f = {}

    f['id'] = id if id else form.__class__.__name__
    f['method'] = method
    f['action'] = action if action else request.path

    if enctype:
        f['enctype'] = enctype

    fields = odict()

    for elem in form:

        fields[elem.name] = {
            'template': ['form-item-'+elem.type+'.jinja', 'form-item-default.jinja'],
            'value': elem
        }

    f['fields'] = fields

    return element('form', f, **kwargs)


def ulist(items, **kwargs):

    value = {
        'items': items
    }

    return element('ulist', value, **kwargs)


def menu(items, **kwargs):

    value = {
        'items': items
    }

    return element('menu', value, **kwargs)
